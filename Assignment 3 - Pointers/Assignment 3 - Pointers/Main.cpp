
// Assignment 3 - Pointers
// James Chartraw


#include <iostream>
#include <conio.h>


using namespace std;

// The SwapIntegers function swaps the first number
// and the second number. 

int swapIntegers(int *pfirst, int *psecond)
{
	*psecond = *pfirst + *psecond;
	*pfirst = *psecond - *pfirst;
	*psecond = *psecond - *pfirst;
	return *pfirst;
	return *psecond;
}


// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	swapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	_getch();
	return 0;
}
